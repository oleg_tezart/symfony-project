<?php

namespace UsersBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use UsersBundle\Form\Type\AdminProfileType;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('UsersBundle:Default:index.html.twig');
    }

    public function UsersInfoAction()
    {
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $user = $this->get('security.token_storage')->getToken()->getUser();
        } else {
            $user = null;
        }
        return $this->render('UsersBundle:Default:user_info.html.twig', array('user' => $user));
    }

    public function UserAdminInfoAction()
    {
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $user = $this->get('security.token_storage')->getToken()->getUser();
        } else {
            $user = null;
        }
        return $this->render('UsersBundle:Admin:user_admin_info.html.twig', array('user' => $user));
    }
}

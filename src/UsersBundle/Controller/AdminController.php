<?php

namespace UsersBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;

use UsersBundle\Form\Type\UserType;
use UsersBundle\Form\Type\SearchType;

class AdminController extends Controller
{
    public function usersAction(Request $request, $page, $perpage)
    {
        $form = $this->createForm(SearchType::class, NULL);
        $form->handleRequest($request);
        if ($request->getMethod() == 'POST') {
            if ($form->isValid()) {
                $word = $form['search']->getData();
                $query = $this->getDoctrine()->getRepository('UsersBundle:Users')->findUsers($word);
                $paginator = $this->get('knp_paginator');
                $users = $paginator->paginate(
                    $query,
                    $page,
                    $request->query->getInt('limit', $perpage));
            }
        } else {
            $query = $this->getDoctrine()->getRepository('UsersBundle:Users')->findAll();
            $paginator = $this->get('knp_paginator');
            $users = $paginator->paginate(
                $query,
                $page,
                $request->query->getInt('limit', $perpage));
        }

        return $this->render('UsersBundle:Admin:users.html.twig',
            [
                'users' => $users,
                'page' => $page,
                'perpage' => $perpage,
                'form' => $form->createView()
            ]);
    }

    public function editUserAction(Request $request,$id)
    {
        $user = $this->getDoctrine()->getRepository('UsersBundle:Users')->find($id);

        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($request->getMethod() == 'POST')
        {
            if ($form->isValid())
            {
                if(!is_dir("data/users/$id")){mkdir("data/users/$id", 0755);}

                $data = $form->getData();
                $password = $form['new_password']->getData();
                if($password){
                    $encoder = $this->container->get('security.password_encoder');
                    $encoded = $encoder->encodePassword($user, $password);
                    $data->setPassword($encoded);
                }
                $data->getProfile()->setCreation(new \DateTime("now"));
                $data->setRoles(array($data->getRole()));

                $em = $this->getDoctrine()->getManager();
                $em->persist($data);
                $em->flush();

                return $this->redirect($this->generateUrl('admin_users_view_all'));
            }
        }


        return $this->render('UsersBundle:Admin:edit_user.html.twig', array('form' => $form->createView(), 'user' => $user));
    }

    public function deleteUserPhotoAction($id)
    {
        if($id == 0){$id = $this->get('security.token_storage')->getToken()->getUser()->getId();}

        $user = $this->getDoctrine()->getRepository('UsersBundle:Users')->find($id);
        $file = $user->getProfile()->getPath();
        $user->getProfile()->setPath(NULL);

        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        if($file != NULL){ unlink(__DIR__.'/../../../web/data/users/'.$id.'/'.$file); }

        return $this->redirect($this->generateUrl('admin_edit_user', array('id'=>$id)));

    }

    public function deleteUserAction($id)
    {
        $user = $this->getDoctrine()->getRepository('UsersBundle:Users')->find($id);

        $em = $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();

        $directory =  __DIR__.'/../../../web/data/users/'.$id;
        if(is_dir("$directory")){
            array_map('unlink', glob("$directory/*.*"));
            rmdir($directory);
        }

        return $this->redirect($this->generateUrl('admin_users_view_all'));
    }

}

<?php

namespace NewsBundle\Controller;

use AppBundle\Entity\Image;
use NewsBundle\Entity\News;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class NewsController extends Controller
{
    const IMAGE_PREFIX_PATH = 'news/';

    // Admin actions
    public function indexAction(Request $request, $page, $perpage) {
        $em = $this->getDoctrine()->getManager();
        $query = $em->getRepository('NewsBundle:News')->findAll();

        $paginator = $this->get('knp_paginator');
        $news = $paginator->paginate(
            $query,
            $page,
            $request->query->getInt('limit', $perpage)
        );

        return $this->render('NewsBundle:news/admin:index.html.twig', [
            'news' => $news,
            'page' => $page,
            'perpage' => $perpage
        ]);
    }

    public function createAction(Request $request) {
        $news = new News();
        $form = $this->createForm('NewsBundle\Form\NewsType', $news);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $news->prePersist();

            if ($news->getBackgroundFile())
                $this->saveBackground($news);

            $em->persist($news);
            $em->flush();

            return $this->redirectToRoute('news_index');
        }

        return $this->render('NewsBundle:news/admin:create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    public function editAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $news = $em->getRepository('NewsBundle:News')->find($id);
        $form = $this->createForm('NewsBundle\Form\NewsType', $news);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $news->preUpdate();

            //if we get new background Image from user
            if($request->files->get('news')['backgroundFile'] !== null)
                $this->saveBackground($news);

            $em->persist($news);
            $em->flush();

            return $this->redirectToRoute('news_index');
        }

        return $this->render('NewsBundle:news/admin:create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    public function deleteAction($id) {
        $em = $this->getDoctrine()->getManager();
        $news = $em->getRepository('NewsBundle:News')->find($id);
        $em->remove($news);
        $em->flush();

        return $this->redirectToRoute('news_index');
    }

    // Public actions
    public function publicIndexAction(Request $request, $page, $perpage) {
        $query = $this->getDoctrine()->getRepository('NewsBundle:News')->findBy(['status' => 1]);

        $paginator = $this->get('knp_paginator');
        $news = $paginator->paginate(
            $query,
            $page,
            $request->query->getInt('limit', $perpage)
        );

        return $this->render('NewsBundle:news/public:index.html.twig', [
            'news' => $news,
            'page' => $page,
            'perpage' => $perpage
        ]);
    }

    public function publicViewAction($id) {
        $em = $this->getDoctrine()->getManager();
        $news = $em->getRepository('NewsBundle:News')->findOneBy(['slug' => $id, 'status' => 1]);
        $latestNews = $em->getRepository('NewsBundle:News')->findLatestNews($id, 4);

        return $this->render('NewsBundle:news/public:view.html.twig', [
            'news' => $news,
            'latest' => $latestNews
        ]);
    }

    /**
     * @param News $news
     */
    private function saveBackground(News $news) {
        $backgroundFile = $news->getBackgroundFile();
        $em = $this->getDoctrine()->getManager();

        //if we are editing news
        if ($news->getId()) {
            $dbNews = $em->getRepository(News::class)->find($news->getId());
            //than we need to delete our old image
            if ($dbNews->getBackground() && $dbNews->getBackground()->getName() !== '') {
                unlink($dbNews->getBackground()->getAbsolutePath());
                $em->remove($dbNews->getBackground());
                $dbNews->setBackground(null);
            }
        }
        //and save new one
        $background = new Image();
        if ($backgroundFile && $background = $background->saveFile($backgroundFile,
                self::IMAGE_PREFIX_PATH)) {
            $news->setBackground($background);
        }
    }

    // Ajax action
    public function newsRemoveBackgroundAction($id) {
        $em = $this->getDoctrine()->getManager();
        $news = $em->getRepository('NewsBundle:News')->find($id);
        $image = $news->getBackground();

        if (is_file($image->getAbsolutePath())) {
            unlink($image->getAbsolutePath());
        }

        $news->setBackground(null);

        $em->remove($image);
        $em->persist($news);
        $em->flush();

        return new Response('200');
    }
}
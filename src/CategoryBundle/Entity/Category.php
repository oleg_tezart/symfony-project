<?php

namespace CategoryBundle\Entity;

use Behat\Transliterator\Transliterator;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use ProductBundle\Entity\Product;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Category
 *
 * @ORM\Table(name="category")
 * @ORM\Entity(repositoryClass="CategoryBundle\Entity\Repository\CategoryRepository")
 */
class Category {
    use \A2lix\I18nDoctrineBundle\Doctrine\ORM\Util\Translatable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation", type="datetime")
     */
    private $creation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string")
     */
    private $slug;

    /**
     * @var Category
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="children")
     */
    private $parent;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Category", mappedBy="parent", cascade={"remove", "persist"})
     */
    private $children;

    /**
     * @var integer
     * @ORM\Column(name="number", type="integer")
     * @Assert\NotBlank()
     * @Assert\GreaterThanOrEqual(1)
     */
    private $number;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="ProductBundle\Entity\Product", mappedBy="categories", cascade={"persist"})
     */
    private $products;

    private $translations;

    public function __construct() {
        $this->children = new ArrayCollection();
        $this->products = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set creation
     *
     * @param \DateTime $creation
     *
     * @return Category
     */
    public function setCreation($creation) {
        $this->creation = $creation;

        return $this;
    }

    /**
     * Get creation
     *
     * @return \DateTime
     */
    public function getCreation() {
        return $this->creation;
    }

    /**
     * @return Category
     */
    public function getParent() {
        return $this->parent;
    }

    /**
     * @param Category $parent
     */
    public function setParent($parent) {
        $this->parent = $parent;
    }

    /**
     * @return ArrayCollection
     */
    public function getChildren() {
        return $this->children;
    }

    /**
     * @param ArrayCollection $children
     */
    public function setChildren($children) {
        $this->children = $children;
    }

    /**
     * Add child.
     *
     * @param \CategoryBundle\Entity\Category $child
     *
     * @return Category
     */
    public function addChild(Category $child) {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child.
     *
     * @param \CategoryBundle\Entity\Category $child
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeChild(Category $child) {
        return $this->children->removeElement($child);
    }

    /**
     * @return int
     */
    public function getNumber() {
        return $this->number;
    }

    /**
     * @param int $number
     */
    public function setNumber($number) {
        $this->number = $number;
    }

    /**
     * @return string
     */
    public function getName() {
        if (!$this->getCurrentTranslation()) {
            $name = 'No translations found...';
        } else {
            $name = $this->getCurrentTranslation()->getName();
        }
        return $name;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getProducts() {
        return $this->products;
    }

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $products
     */
    public function setProducts($products) {
        $this->products = $products;
    }

    /**
     * Add product.
     *
     * @param \ProductBundle\Entity\Product $product
     *
     * @return Category
     */
    public function addProduct(Product $product) {
        $this->products[] = $product;

        return $this;
    }

    /**
     * Remove product.
     *
     * @param \ProductBundle\Entity\Product $product
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeProduct(Product $product) {
        return $this->products->removeElement($product);
    }

    /**
     * @return \DateTime
     */
    public function getUpdated() {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated($updated) {
        $this->updated = $updated;
    }

    /**
     * @return string
     */
    public function getSlug() {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug) {
        $this->slug = $slug;
    }

    public function prePersist() {
        $this->slug = Transliterator::transliterate($this->getName());
        $this->updated = new \DateTime();
        $this->creation = new \DateTime();
    }

    public function preUpdate() {
        $this->slug = Transliterator::transliterate($this->getName());
        $this->updated = new \DateTime();
    }

    /**
     * @return string
     */
    public function __toString() {
        $name = [];
        foreach ($this->translations as $translation) {
            $name[] = $translation->getName();
        }

        return reset($name);
    }
}

<?php

namespace CategoryBundle\Controller;

use CategoryBundle\Entity\Category;
use CategoryBundle\Form\CategoryType;
use MenuBundle\Entity\Routes;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CategoryController extends Controller {
    public function indexAction(Request $request, $page, $perpage) {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQueryBuilder() //get rid of lazy loading
        ->select('c, ch, t')
            ->from('CategoryBundle:Category', 'c')
            ->leftJoin('c.children', 'ch')
            ->leftJoin('c.translations', 't')
            ->orderBy('c.number')
            ->getQuery()
            ->getResult();

        $paginator = $this->get('knp_paginator');
        $categories = $paginator->paginate(
            $query,
            $page,
            $request->query->getInt('limit', $perpage)
        );

        return $this->render('CategoryBundle::index.html.twig', [
            'categories' => $categories,
            'page'       => $page,
            'perpage'    => $perpage
        ]);
    }

    public function createAction(Request $request) {
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $category->prePersist();
            $em->persist($category);
            $em->flush();

            // Set route for menu
            $route = new Routes();
            $route->setModule('category');
            $route->setRoute('category_view');
            $route->setParamId($category->getId());
            $route->setParamSlug($category->getSlug());

            $em->persist($route);
            $em->flush();

            return $this->redirectToRoute('category_homepage');
        }

        return $this->render('CategoryBundle::create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    public function editAction(Request $request, $id) {
        $category = $this->getDoctrine()->getRepository(Category::class)->find($id);
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $category->preUpdate();
            $em->persist($category);
            $em->flush();

            return $this->redirectToRoute('category_homepage');
        }

        return $this->render('@Category/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    public function deleteAction($id) {
        $category = $this->getDoctrine()->getRepository(Category::class)->find($id);

        $em = $this->getDoctrine()->getManager();
        $em->remove($category);
        $em->flush();

        // Delete rout end item menu
        $route = $em->getRepository(Routes::class)->findOneBy(['param_id' => $id]);
        if (!empty($route)) {
            $em->remove($route);
            $em->flush();
        }

        return $this->redirectToRoute('category_homepage');
    }

    public function getForSelectAction(Request $request) {
        $search = $request->get('q');

        $categories = $this->getDoctrine()->getRepository(Category::class)->findAllTranslationsByName($search);
        $result = array();

        foreach ($categories as $category) {
            $result[] = [
                'id'   => $category->getId(),
                'text' => $category->getName()
            ];
        }

        return new Response(json_encode($result));
    }

    public function viewAction($id) {
        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository(Category::class)->findOneBy(['slug' => $id]);

        return $this->render('@Category/view.html.twig', [
            'category' => $category
        ]);
    }
}

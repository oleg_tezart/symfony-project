<?php

namespace MenuBundle\DataFixtures;

use MenuBundle\Entity\Items;
use MenuBundle\Entity\ItemsTranslation;
use MenuBundle\Entity\Routes;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class RouteFixture implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /*$route = new Routes();
        $route->setModule('app');
        $route->setRoute('admin_users_view_all');
        $manager->persist($route);*/

        /*$route = new Routes();
        $route->setModule('app');
        $route->setRoute('page');
        $manager->persist($route);*/

        $item = new Items();
        $item->setType(1);
        $item->setSequence(5);
        $item->setFavicon('fa fa-cogs');
        $translation = new ItemsTranslation();
        $translation->setLocale('uk');
        $translation->setTitle('Властивості товара');
        $item->addTranslation($translation);

        $manager->persist($item);
        $manager->flush();

        $this->createItem($manager, 'app', 'homepage', 'fa fa-reply', 'Головна', 1, 1);
        $this->createItem($manager, 'app', 'admin', null, 'Адмінка', 1, 2);
        $this->createItem($manager, 'news', 'news_public_index', null, 'Новини', 2, 2);
        $this->createItem($manager, 'generals', 'general_settings', 'fa fa-wrench', 'Загальні налаштування', 2, 1);
        $this->createItem($manager, 'menu', 'menu', 'fa fa-bars', 'Меню', 3, 1);
        $this->createItem($manager, 'category', 'category_homepage', 'fa fa-list-alt', 'Категорії', 4, 1);
        $this->createItem($manager, 'product', 'product_homepage', 'fa fa-product-hunt', 'Товари', 5, 1);
        $this->createItem($manager, 'order', 'order_homepage', 'fa fa-archive', 'Закази', 6, 1);

        $item = $manager->getRepository(Items::class)->findOneBy(['route' => null, 'sequence' => 5]);

        $this->createItem($manager, 'size', 'size_homepage', 'fa fa-arrows-h', 'Розміри', 1, 1, $item);
        $this->createItem($manager, 'color', 'color_homepage', 'fa fa-adjust', 'Кольори', 2, 1, $item);
        $this->createItem($manager, 'news', 'news_index', 'fa fa-newspaper-o', 'Новини', 6, 1);

        $manager->flush();

    }

    public function createItem($manager, $module, $routeName, $favicon, $title, $sequence, $menuType, $parentId = null) {
        $route = new Routes();
        $route->setModule($module);
        $route->setRoute($routeName);
        $manager->persist($route);
        $item = new Items();
        $item->setRoute($route);
        $item->setSequence($sequence);
        $item->setType($menuType);
        $item->setFavicon($favicon);

        if ($parentId != null)
            $item->setParent($parentId);

        $translation = new ItemsTranslation();
        $translation->setLocale('uk');
        $translation->setTitle($title);
        $item->addTranslation($translation);
        $manager->persist($item);
    }
}
<?php

namespace MenuBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Posts
 *
 * @ORM\Entity(repositoryClass="MenuBundle\Entity\Repository\ItemsRepository")
 * @ORM\Table(name="menu_items")
 * @ORM\HasLifecycleCallbacks()
 */
class Items
{
    use \A2lix\I18nDoctrineBundle\Doctrine\ORM\Util\Translatable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="type", type="integer")
     */
    protected $type;

    /**
     * @ORM\Column(name="pid", type="integer", nullable=true)
     */
    protected $pid;

    /**
     * @ORM\Column(name="sequence", type="integer")
     */
    protected $sequence;

    /**
     * @ORM\Column(name="favicon", type="string", nullable=true)
     */
    protected $favicon;

    /**
     * @ORM\OneToOne(targetEntity="Routes", inversedBy="item")
     * @ORM\JoinColumn(name="route", referencedColumnName="id")
     */
    protected $route;

    /**
     * @ORM\Column(name="creation", type="datetime")
     */
    protected $creation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Items", mappedBy="parent", cascade={"remove", "persist"})
     */
    private $childrens;

    /**
     * @ORM\ManyToOne(targetEntity="Items", inversedBy="childrens")
     * @ORM\JoinColumn(name="pid", referencedColumnName="id")
     */
    private $parent;

    private $translations;

    public function __construct()
    {
        $this->childrens = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        $title = [];
        foreach ($this->translations as $translation)
        {
            $title[] = $translation->getTitle();
        }

        return reset($title);
    }

    public function getTitle()
    {
        if(!$this->getCurrentTranslation()){
            $title = 'No translations found...';
        } else {
            $title = $this->getCurrentTranslation()->getTitle();
        }
        return $title;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pid
     *
     * @param integer $pid
     *
     * @return Items
     */
    public function setPid($pid)
    {
        $this->pid = $pid;

        return $this;
    }

    /**
     * Get pid
     *
     * @return integer
     */
    public function getPid()
    {
        return $this->pid;
    }

    /**
     * Set sequence
     *
     * @param integer $sequence
     *
     * @return Items
     */
    public function setSequence($sequence)
    {
        $this->sequence = $sequence;

        return $this;
    }

    /**
     * Get sequence
     *
     * @return integer
     */
    public function getSequence()
    {
        return $this->sequence;
    }

    /**
     * Set creation
     *
     * @param \DateTime $creation
     *
     * @return Items
     */
    public function setCreation($creation)
    {
        $this->creation = $creation;

        return $this;
    }

    /**
     * Get creation
     *
     * @return \DateTime
     */
    public function getCreation()
    {
        return $this->creation;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated() {
        return $this->updated;
    }

    /**
     * @ORM\PreUpdate()
     */
    public function setUpdatedTime() {
        $this->updated = new \DateTime('now');
    }

    /**
     * @ORM\PrePersist()
     */
    public function setCreatedTime() {
        $this->creation = new \DateTime('now');
        $this->updated = new \DateTime('now');
    }

    /**
     * Set route
     *
     * @param \MenuBundle\Entity\Routes $route
     *
     * @return Items
     */
    public function setRoute(\MenuBundle\Entity\Routes $route = null)
    {
        $this->route = $route;

        return $this;
    }

    /**
     * Get route
     *
     * @return \MenuBundle\Entity\Routes
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * Add children
     *
     * @param \MenuBundle\Entity\Items $children
     *
     * @return Items
     */
    public function addChildren(\MenuBundle\Entity\Items $children)
    {
        $this->childrens[] = $children;

        return $this;
    }

    /**
     * Remove children
     *
     * @param \MenuBundle\Entity\Items $children
     */
    public function removeChildren(\MenuBundle\Entity\Items $children)
    {
        $this->childrens->removeElement($children);
    }

    /**
     * Get childrens
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildrens()
    {
        return $this->childrens;
    }

    /**
     * Set parent
     *
     * @param \MenuBundle\Entity\Items $parent
     *
     * @return Items
     */
    public function setParent(\MenuBundle\Entity\Items $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \MenuBundle\Entity\Items
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set menu
     *
     * @param integer $type
     *
     * @return Items
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getFavicon()
    {
        return $this->favicon;
    }

    /**
     * @param string $favicon
     */
    public function setFavicon($favicon)
    {
        $this->favicon = $favicon;
    }


}

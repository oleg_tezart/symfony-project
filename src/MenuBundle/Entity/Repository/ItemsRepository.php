<?php

namespace MenuBundle\Entity\Repository;

/**
 * ItemsRepository
 */
class ItemsRepository extends \Doctrine\ORM\EntityRepository {
    /**
     * @return mixed
     */
    public function findAllWithNullPidExceptId($editItemId, $menuType) {
        return $this->getEntityManager()
            ->createQuery("SELECT i FROM MenuBundle:Items i WHERE i.id != $editItemId AND i.pid IS NULL AND i.type = $menuType AND i.route IS NULL")
            ->getResult();
    }

    public function findAllWithPidExceptId($editItemId, $id, $menuType) {
        return $this->getEntityManager()
            ->createQuery("SELECT i FROM MenuBundle:Items i WHERE i.id != $editItemId AND i.pid = $id AND i.type = $menuType AND i.route IS NULL")
            ->getResult();
    }
}

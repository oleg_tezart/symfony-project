<?php

namespace ProductBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Size
 *
 * @ORM\Table(name="size")
 * @ORM\Entity(repositoryClass="ProductBundle\Entity\Repository\SizeRepository")
 */
class Size {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation", type="datetime")
     */
    private $creation;

    /**
     * @var |DateTime
     *
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", unique=true)
     * @Assert\NotBlank()
     * @Assert\Length(min="1")
     */
    private $name;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="ProductBundle\Entity\Product", mappedBy="sizes")
     */
    private $products;

    /**
     * Constructor
     */
    public function __construct() {
        $this->products = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set creation
     *
     * @param \DateTime $creation
     *
     * @return Size
     */
    public function setCreation($creation) {
        $this->creation = $creation;

        return $this;
    }

    /**
     * Get creation
     *
     * @return \DateTime
     */
    public function getCreation() {
        return $this->creation;
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name) {
        $this->name = $name;
    }

    /**
     * @return ArrayCollection
     */
    public function getProducts() {
        return $this->products;
    }

    /**
     * @param ArrayCollection $products
     */
    public function setProducts($products) {
        $this->products = $products;
    }

    /**
     * Add product.
     *
     * @param \ProductBundle\Entity\Product $product
     *
     * @return Size
     */
    public function addProduct(Product $product) {
        $this->products[] = $product;

        return $this;
    }

    /**
     * Remove product.
     *
     * @param \ProductBundle\Entity\Product $product
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeProduct(Product $product) {
        return $this->products->removeElement($product);
    }

    /**
     * @return mixed
     */
    public function getUpdated() {
        return $this->updated;
    }

    /**
     * @param mixed $updated
     */
    public function setUpdated($updated) {
        $this->updated = $updated;
    }

    /**
     * @ORM\PreUpdate()
     */
    public function setUpdatedTime() {
        $this->updated = new \DateTime();
    }

    /**
     * @return string
     */
    public function __toString() {
        return $this->name;
    }

    public function prePersist() {
        $this->updated = new \DateTime();
        $this->creation = new \DateTime();
    }

    public function preUpdate() {
        $this->updated = new \DateTime();
    }
}

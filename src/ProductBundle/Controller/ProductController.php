<?php

namespace ProductBundle\Controller;

use AppBundle\Entity\Image;
use ProductBundle\Entity\Product;
use ProductBundle\Form\ProductType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ProductController extends Controller {
    const IMAGE_PREFIX_PATH = 'products/';

    public function indexAction(Request $request, $page, $perpage) {
        $em = $this->getDoctrine()->getManager();

        $query = $em->createQueryBuilder() //get rid of lazy loading
            ->select('p, cat, col, s, i, t')
            ->from('ProductBundle:Product', 'p')
            ->leftJoin('p.categories', 'cat')
            ->leftJoin('p.sizes', 's')
            ->leftJoin('p.colors', 'col')
            ->leftJoin('p.mainImage', 'i')
            ->leftJoin('p.translations', 't')
            ->getQuery()
            ->getResult();

        $paginator = $this->get('knp_paginator');
        $products = $paginator->paginate(
            $query,
            $page,
            $request->query->getInt('limit', $perpage)
        );

        return $this->render('ProductBundle:products:index.html.twig', [
            'products' => $products,
            'page'     => $page,
            'perpage'  => $perpage
        ]);
    }

    public function createAction(Request $request) {
        $product = new Product();
        $form = $this->createForm(ProductType::class, $product);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $product->prePersist();

            if ($product->getMainImageFile())
                $this->saveMainImage($product);

            foreach ($product->getFiles() as $file) {
                $image = new Image();
                $image = $image->saveFile($file, self::IMAGE_PREFIX_PATH . $product->getId() . '/');
                $image->setProduct($product);
                $product->addImage($image);
            }

            $em->persist($product);
            $em->flush();

            return $this->redirectToRoute('product_homepage');
        }

        return $this->render('ProductBundle:products:create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    public function editAction(Request $request, $id) {
        $product = $this->getDoctrine()->getManager()->createQueryBuilder()
            ->select('p, cat, col, s, i, t, sim')
            ->from('ProductBundle:Product', 'p')
            ->leftJoin('p.categories', 'cat')
            ->leftJoin('p.similars', 'sim')
            ->leftJoin('p.sizes', 's')
            ->leftJoin('p.colors', 'col')
            ->leftJoin('p.images', 'i')
            ->leftJoin('p.translations', 't')
            ->where('p.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getSingleResult();

        $form = $this->createForm(ProductType::class, $product);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $product->preUpdate();

            //if we get new main Image from user
            if ($request->files->get('product')['mainImageFile'] !== null)
                $this->saveMainImage($product);

            $em->persist($product);
            $em->flush();

            return $this->redirectToRoute('product_homepage');
        }

        return $this->render('ProductBundle:products:create.html.twig', [
            'form'        => $form->createView(),
            'imagesArray' => $this->formImageArray($product->getImages())
        ]);
    }

    public function deleteAction($id) {
        $product = $this->getDoctrine()->getRepository(Product::class)->find($id);
        $em = $this->getDoctrine()->getManager();

        $em->remove($product);
        $em->flush();

        return $this->redirectToRoute('product_homepage');
    }

    public function getProductsForSelectAction(Request $request) {
        $search = $request->get('q');

        $products = $this->getDoctrine()->getRepository(Product::class)->findAllTranslationsByName($search);
        $result = array();

        foreach ($products as $product) {
            $result[] = [
                'id'   => $product->getId(),
                'text' => $product->getName()
            ];
        }

        return new Response(json_encode($result));
    }

    //====================================AJAX Actions================================================//
    public function productAddImageAction($id) {
        $original_name = $_FILES['product']['name']['files'][0];
        $tmp_name = $_FILES['product']['tmp_name']['files'][0];
        $file = new UploadedFile($tmp_name, $original_name);
        $image = new Image();
        $imagesArray = array();

        if ($id && $id != 0) {
            $image = $image->saveFile($file, self::IMAGE_PREFIX_PATH . $id . '/');
            $em = $this->getDoctrine()->getManager();
            $product = $this->getDoctrine()->getRepository('ProductBundle:Product')->find($id);

            $image->setProduct($product);
            $product->addImage($image);


            $em->persist($product);
            $em->flush();
        }

        $imagesArray[] = array(
            "name" => $image->getName(),
            "type" => $file->getClientMimeType(),
            "file" => $image->getWebPath(),
            "data" => array("url" => $image->getWebPath())
        );

        return new Response(json_encode($imagesArray));
    }

    public function productRemoveMainImageAction($id) {
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository(Product::class)->find($id);
        $image = $product->getMainImage();

        if (is_file($image->getAbsolutePath())) {
            unlink($image->getAbsolutePath());
        }

        $product->setMainImage(null);

        $em->remove($image);
        $em->persist($product);
        $em->flush();

        return new Response('200');
    }

    public function productRemoveImageAction() {
        $name = $_POST['file'];
        $image = $this->getDoctrine()->getRepository('AppBundle:Image')->findOneBy(['name' => $name]);

        if (is_file($image->getAbsolutePath())) {
            unlink($image->getAbsolutePath());
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($image);
        $em->flush();

        return new Response('200');
    }

    //====================================End of AJAX Actions================================================//

    public function viewAction($product_slug) {
        $em = $this->getDoctrine()->getManager();

        $product = $em->createQueryBuilder()
            ->select('p, col, s, i, im, t, sim')
            ->from('ProductBundle:Product', 'p')
            ->leftJoin('p.similars', 'sim')
            ->leftJoin('p.sizes', 's')
            ->leftJoin('p.colors', 'col')
            ->leftJoin('p.images', 'i')
            ->leftJoin('p.mainImage', 'im')
            ->leftJoin('p.translations', 't')
            ->where('p.slug = :slug')
            ->setParameter('slug', $product_slug)
            ->getQuery()
            ->getSingleResult();

        return $this->render('@Product/products/view.html.twig', [
            'product' => $product
        ]);
    }

    /**
     * @param $product Product
     */
    private function saveMainImage(Product $product) {
        $mainImageFile = $product->getMainImageFile();
        $em = $this->getDoctrine()->getManager();

        //if we are editing product
        if ($product->getId()) {
            $dbProduct = $em->getRepository(Product::class)->find($product->getId());
            //than we need to delete our old image
            if ($dbProduct->getMainImage() && $dbProduct->getMainImage()->getName() !== '') {
                unlink($dbProduct->getMainImage()->getAbsolutePath());
                $em->remove($dbProduct->getMainImage());
                $dbProduct->setMainImage(null);
            }
        }
        //and save new one
        $mainImage = new Image();
        if ($mainImageFile && $mainImage = $mainImage->saveFile($mainImageFile,
                self::IMAGE_PREFIX_PATH . $product->getId() . '/')) {
            $product->setMainImage($mainImage);
        }
    }

    /**
     * @param $images
     *
     * @return array|string
     */
    private function formImageArray($images) {
        $imagesArray = [];
        foreach ($images as $image) {
            $file = new UploadedFile($image->getAbsolutePath(), $image->getName());

            $imagesArray[] = array(
                "name" => $image->getName(),
                "type" => $file->getType(),
                "size" => $file->getSize(),
                "file" => $image->getWebPath(),
                "data" => array("url" => $image->getWebPath())
            );
        }
        $imagesArray = json_encode($imagesArray);
        return $imagesArray;
    }
}
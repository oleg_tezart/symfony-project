<?php

namespace ProductBundle\Form;

use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class ProductType extends AbstractType {
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('translations', TranslationsType::class, array(
                'fields' => array(
                    'name' => array(
                        'label'      => 'Name',
                        'field_type' => TextType::class,
                        'required'   => true
                    ),
                ),
                'label'  => false
            ))
            ->add('content')
            ->add('vendor')
            ->add('price', MoneyType::class, [
                'currency' => 'USD'
            ])
            ->add('status', CheckboxType::class, ['required' => false])
            ->add('similars', Select2EntityType::class, [
                'remote_route'         => 'product_similiar_search_json',
                'multiple'             => true,
                'class'                => 'ProductBundle\Entity\Product',
                'minimum_input_length' => 0,
                'page_limit'           => 50,
                'required'             => false,
                'allow_clear'          => true,
                'cache'                => true,
                'cache_timeout'        => 60000,
                'scroll'               => true,
                'width'                => '100%'
            ])
            ->add('categories', Select2EntityType::class, [
                'remote_route'         => 'category_name_search_json',
                'multiple'             => true,
                'class'                => 'CategoryBundle\Entity\Category',
                'minimum_input_length' => 0,
                'page_limit'           => 50,
                'required'             => true,
                'allow_clear'          => true,
                'cache'                => true,
                'cache_timeout'        => 60000,
                'scroll'               => true,
                'width'                => '100%'
            ])
            ->add('colors', Select2EntityType::class, [
                'remote_route'         => 'color_name_search_json',
                'multiple'             => true,
                'class'                => 'ProductBundle\Entity\Color',
                'minimum_input_length' => 0,
                'page_limit'           => 50,
                'required'             => true,
                'allow_clear'          => true,
                'cache'                => true,
                'cache_timeout'        => 60000,
                'scroll'               => true,
                'width'                => '100%'
            ])
            ->add('sizes', Select2EntityType::class, [
                'remote_route'         => 'size_name_search_json',
                'multiple'             => true,
                'class'                => 'ProductBundle\Entity\Size',
                'minimum_input_length' => 0,
                'page_limit'           => 50,
                'required'             => true,
                'allow_clear'          => true,
                'cache'                => true,
                'cache_timeout'        => 60000,
                'scroll'               => true,
                'width'                => '100%'
            ])
            ->add('files', FileType::class, [
                'multiple'   => true,
                'required'   => false,
                'data_class' => null
            ])
            ->add('mainImageFile', FileType::class, [
                'required'   => false,
                'data_class' => null
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'ProductBundle\Entity\Product'
        ));
    }
}

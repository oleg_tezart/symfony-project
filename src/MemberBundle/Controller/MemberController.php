<?php

namespace MemberBundle\Controller;

use AppBundle\Entity\Image;
use MemberBundle\Entity\Member;
use MemberBundle\Form\MemberType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class MemberController extends Controller
{
    const IMAGE_PREFIX_PATH = 'member/';

    public function createAction(Request $request) {
        $member = new Member();
        $form = $this->createForm("MemberBundle\Form\MemberType", $member);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $member->prePersist();

            if ($member->getImageFile())
                $this->saveImage($member);

            $em->persist($member);
            $em->flush();

            return $this->redirectToRoute('admin');
        }

        return $this->render('MemberBundle:member/admin:create.html.twig', [
            'form' => $form->createView()
        ]);
    }



    /**
     * @param Member $member
     */
    private function saveImage(Member $member) {
        $imageFile = $member->getImageFile();
        $em = $this->getDoctrine()->getManager();

        //if we are editing member
        if ($member->getId()) {
            $dbNews = $em->getRepository(Member::class)->find($member->getId());
            //than we need to delete our old image
            if ($dbNews->getImage() && $dbNews->getImage()->getName() !== '') {
                unlink($dbNews->getImage()->getAbsolutePath());
                $em->remove($dbNews->getImage());
                $dbNews->setImage(null);
            }
        }
        //and save new one
        $image = new Image();
        if ($imageFile && $image = $image->saveFile($imageFile,
                self::IMAGE_PREFIX_PATH)) {
            dump($image);
            $member->setImage($image);
        }
    }
}
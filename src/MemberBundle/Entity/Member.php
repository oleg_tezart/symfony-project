<?php

namespace MemberBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Member
 *
 * @ORM\Table(name="member")
 * @ORM\Entity(repositoryClass="MemberBundle\Entity\Repository\MemberRepository")
 */
class Member
{
    use \A2lix\I18nDoctrineBundle\Doctrine\ORM\Util\Translatable;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Image
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Image", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="image_id", referencedColumnName="id", nullable=true)
     */
    private $image;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation", type="datetime")
     */
    private $creation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;

    //Property for image
    /**
     * @Assert\File(
     *   maxSize="10M",
     *   mimeTypes = {
     *          "image/png",
     *          "image/jpeg",
     *          "image/jpg",
     *          "image/gif"
     *          },
     *   mimeTypesMessage="Allowed types: jpg, jpeg, png, gif"
     *  )
     */
    private $imageFile;

    private $translations;

    /**
     * @return \AppBundle\Entity\Image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param \AppBundle\Entity\Image $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        $name = [];
        foreach ($this->translations as $translation)
        {
            $name[] = $translation->getName();
        }

        return reset($name);
    }

    public function getName()
    {
        if(!$this->getCurrentTranslation()){
            $name = 'No translations found...';
        } else {
            $name = $this->getCurrentTranslation()->getName();
        }
        return $name;
    }

    public function getPosition()
    {
        if(!$this->getCurrentTranslation()){
            $position = 'No translations found...';
        } else {
            $position = $this->getCurrentTranslation()->getPosition();
        }
        return $position;
    }

    /**
     * Set creation.
     *
     * @param \DateTime $creation
     *
     * @return Member
     */
    public function setCreation($creation) {
        $this->creation = $creation;

        return $this;
    }

    /**
     * Get creation.
     *
     * @return \DateTime
     */
    public function getCreation() {
        return $this->creation;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return Member
     */
    public function setUpdated($updated) {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated() {
        return $this->updated;
    }

    public function prePersist() {
        $this->updated = new \DateTime();
        $this->creation = new \DateTime();
    }

    public function preUpdate() {
        $this->updated = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param mixed $imageFile
     */
    public function setImageFile($imageFile)
    {
        $this->imageFile = $imageFile;
    }


}

<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GeneralSettings
 *
 * @ORM\Table(name="general_settings")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 * @package AppBundle\Entity
 */
class GeneralSettings {
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation", type="datetime")
     */
    private $creation;

    /**
     * @var string
     *
     * @ORM\Column(name="facebook", type="string", nullable=true)
     */
    private $facebook;

    /**
     * @var string
     *
     * @ORM\Column(name="twitter", type="string", nullable=true)
     */
    private $twitter;

    /**
     * @var string
     *
     * @ORM\Column(name="instagram", type="string", nullable=true)
     */
    private $instagram;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", nullable=true)
     */
    private $location;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", nullable=true)
     */
    private $telephone;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="admin_email", type="string", nullable=true)
     */
    private $adminEmail;

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated() {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated($updated) {
        $this->updated = $updated;
    }

    /**
     * @return \DateTime
     */
    public function getCreation() {
        return $this->creation;
    }

    /**
     * @param \DateTime $creation
     */
    public function setCreation($creation) {
        $this->creation = $creation;
    }

    /**
     * @return string
     */
    public function getFacebook() {
        return $this->facebook;
    }

    /**
     * @param string $facebook
     */
    public function setFacebook($facebook) {
        $this->facebook = $facebook;
    }

    /**
     * @return string
     */
    public function getTwitter() {
        return $this->twitter;
    }

    /**
     * @param string $twitter
     */
    public function setTwitter($twitter) {
        $this->twitter = $twitter;
    }

    /**
     * @return string
     */
    public function getInstagram() {
        return $this->instagram;
    }

    /**
     * @param string $instagram
     */
    public function setInstagram($instagram) {
        $this->instagram = $instagram;
    }

    /**
     * @return string
     */
    public function getLocation() {
        return $this->location;
    }

    /**
     * @param string $location
     */
    public function setLocation($location) {
        $this->location = $location;
    }

    /**
     * @return string
     */
    public function getTelephone() {
        return $this->telephone;
    }

    /**
     * @param string $telephone
     */
    public function setTelephone($telephone) {
        $this->telephone = $telephone;
    }

    /**
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email) {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getAdminEmail() {
        return $this->adminEmail;
    }

    /**
     * @param string $adminEmail
     */
    public function setAdminEmail($adminEmail) {
        $this->adminEmail = $adminEmail;
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist() {
        $this->updated = new \DateTime();
        $this->creation = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function preUpdate() {
        $this->updated = new \DateTime();
    }
}